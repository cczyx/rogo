<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['passwordrequired'] = 'Password required';
$string['specificpassword'] = 'There is a specific password assigned to this paper.';
$string['enterpw'] = 'Please enter the password for this paper';
$string['pwcontinue'] = 'Continue';
$string['denied_location'] = 'Access to this paper is not permitted from your current location.';
$string['error_time'] = 'The paper you are attempting to access is only available between %s and %s';
$string['nomodules'] = '%s %s (%s) is not registered to take this paper.';
$string['notregistered'] = '%s %s (%s) is not registered on following modules: %s.';
$string['error_module'] = 'This paper is not on any module.';
$string['error_metadata'] = '%s %s (%s) metadata does not match <strong>%s: %s</strong>';
$string['alreadycompleted'] = 'Assessment has already been completed at <strong>%s</strong>.';
$string['ipmismatchtitle'] = 'Please logout of this device';
$string['ipmismatchblurb'] = '%s %s (%s) you appear to be logged into this exam on %s. This device is blocked from proceeding in this exam.<br/>Notify the invigilator if you are unsure what actions to take.';
