$(function() {
  $('#edit_form').submit(function () {
    triggerSave();
  })
  $('#edit_form').validate({
    ignore: '',
    rules: {
      leadin: 'required',
      option_text1: 'required',
      option_text2: 'required'
    },
    messages: {
      leadin: lang['enterleadin'],
      option_text1: '<br />'+lang['enteroptiontext'],
      option_text2: '<br />'+lang['enteroptiontext']
    },
    errorPlacement: function(error, element) {
      if (element.attr('name') == 'leadin') {
        error.insertAfter('#leadin_parent');
        $('#leadin_tbl').css({'border-color' : '#C00000'});
        $('#leadin_tbl').css({'box-shadow' : '0 0 6px rgba(200, 0, 0, 0.85)'});
      } else {
        error.insertAfter(element);
      }
    },
    invalidHandler: function() {
      alert(lang['validationerror']);
    }
  });
});