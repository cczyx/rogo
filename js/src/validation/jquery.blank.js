$(function() {
  $('#edit_form').submit(function () { 
    triggerSave();
  })
  $('#edit_form').validate({
    ignore: '',
    rules: {
      leadin: 'required',
      option_text: 'required'
    },
    messages: {
      leadin: lang['enterleadin'],
      option_text: lang['enterquestion']
    },
    errorPlacement: function(error, element) {
      if (element.attr('name') == 'leadin') {
        error.insertAfter('#leadin_parent');
        $('#leadin_tbl').css({'border-color' : '#C00000'});
        $('#leadin_tbl').css({'box-shadow' : '0 0 6px rgba(200, 0, 0, 0.85)'});
      } else if (element.attr('name') == 'option_text') {
        error.insertAfter('#option_text_parent');
        $('#option_text_tbl').css({'border-color' : '#C00000'});
        $('#option_text_tbl').css({'box-shadow' : '0 0 6px rgba(200, 0, 0, 0.85)'});
      } else {
        error.insertAfter(element);
      }
    },
    invalidHandler: function() {
      alert(lang['validationerror']);
    }
  });
})