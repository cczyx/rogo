var MathJax = {
    extensions: ["tex2jax.js"],
    jax: ["input/TeX", "output/HTML-CSS"],
    messageStyle: "none",
    showMathMenu: false,
    showMathMenuMSIE: false,
    tex2jax: {inlineMath: [['$$','$$'], ['[tex]', '[/tex]'], ['[texi]', '[/texi]']], displayMath: [['$$$','$$$']]},
    "HTML-CSS": {fonts: ["TeX"]}
};
